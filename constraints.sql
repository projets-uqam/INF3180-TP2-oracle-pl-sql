ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY'
/
SET SERVEROUTPUT ON
/
SET ECHO ON
/
/* C1 Contraintes d’intégrité et tests */

/* Contrainte C1: C1MaxInscriptions */

ALTER TABLE GROUPECOURS
ADD (CONSTRAINT C1MaxInscriptions CHECK (maxInscriptions <= 120));

/* C1_C1MaxInscriptions TEST1 «INSERT-FALSE» */
/* PLUS de 120 inscriptions par Groupe-Cours */

INSERT INTO GROUPECOURS VALUES('INF3180',10,12004,125,'DEVL2');

/* C1_C1MaxInscriptions TEST2 «UPDATE-FALSE» */
/* PLUS de 120 inscriptions par Groupe-Cours */

UPDATE GROUPECOURS SET MAXINSCRIPTIONS = 121
WHERE SIGLE LIKE 'INF1110' AND CODEPROFESSEUR LIKE 'TREJ4';

/* Contrainte C2: C2DurationSession */

CREATE OR REPLACE TRIGGER C2DurationSession
 BEFORE INSERT OR UPDATE ON SESSIONUQAM FOR EACH ROW
 WHEN((TO_DATE(NEW.DATEFIN,'DD/MM/YYYY') -
TO_DATE(NEW.DATEDEBUT,'DD/MM/YYYY') < 90))
 BEGIN
 raise_application_error(-20501, 'La date de fin de session doit etre de
90 jours superieure a la date de debut de session');
 END;
/
/* C2_C2DurationSession TEST1 «INSERT-FALSE» */
/* La date de fin de session est 61 jours supérieur à la date de début */

INSERT INTO SessionUQAM VALUES(32003,'3/11/2003','17/12/2003');

/* C2_C2DurationSession TEST2 «UPDATE-FALSE» */
/* La date de fin de session est 59 jours supérieur à la date de début */

UPDATE SessionUQAM SET DATEDEBUT = '4/03/2004' WHERE CODESESSION = 12004;

/* Contrainte C3: C3DateAbandon */

 CREATE OR REPLACE TRIGGER C3DateAbandon
 BEFORE INSERT OR UPDATE ON INSCRIPTION FOR EACH ROW
 DECLARE
 uneDateFin  DATE;
 uneDateDebut  DATE;
 minJours INTEGER := 30;
 BEGIN
 SELECT DATEFIN INTO uneDateFin FROM SESSIONUQAM WHERE CODESESSION =:NEW.CODESESSION;
 SELECT DATEDEBUT INTO uneDateDebut FROM SESSIONUQAM WHERE CODESESSION =:NEW.CODESESSION;
 IF(TO_DATE(:NEW.DATEABANDON,'DD/MM/YYYY') > uneDateFin )THEN
 raise_application_error (-20502,'La date d’abandon d’un cours doit etre
 toujours inferieur a la date de fin de session.');
 ELSE IF (TO_DATE(:NEW.DATEABANDON,'DD/MM/YYYY') - uneDateDebut < minJours) THEN
 raise_application_error (-20503, 'La date d’abandon d’un cours doit etre
 de 30 jours superieur a la date de debut de session');
 END IF;
 END IF;
 END;
/

/* C3DateAbandon TEST1 «INSERT-FALSE» */

/* La date d’abandon est de 7 jours supérieur à la date de début de session */

INSERT INTO Inscription
VALUES('VANV05127201','INF1110',20,32003,'16/08/2003','10/09/2003',null);

/* C3DateAbandon TEST2 «UPDATE-FALSE» */

/* La date d’abandon est supérieur à la date de début de session */

UPDATE INSCRIPTION SET DATEABANDON = '17/05/2006'
WHERE CODEPERMANENT = 'VANV05127201' AND CODESESSION = 32003;

/* Contrainte C4: C4NoteNull */

 CREATE OR REPLACE TRIGGER C4NoteNull
 BEFORE INSERT OR UPDATE ON INSCRIPTION FOR EACH ROW
 BEGIN
 IF (:NEW.DATEABANDON IS NOT NULL AND :NEW.NOTE IS NOT NULL) THEN
 raise_application_error(-20504, 'Si la date d’abandon est non nulle, la note doit etre nulle');
 END IF;
 END;
/
/* C4NoteNull TEST1 «INSERT-FALSE» */

/* La date d’abandon est NON nulle et la note est NON nulle */

INSERT INTO Inscription
VALUES('MARA25087501','INF3180',30,32003,'16/08/2003','20/09/2003',90);

/* C4NoteNull TEST2 «UPDATE-FALSE» */

/* La date d’abandon est NON nulle et la note est NON nulle */

UPDATE INSCRIPTION SET NOTE = 80 WHERE
CODEPERMANENT = 'MONC05127201' AND CODESESSION = 12004 AND SIGLE = 'INF5180';

/* Contrainte C5: C5CoursSuprimé */

 CREATE OR REPLACE TRIGGER C5CoursSuprime
 AFTER DELETE ON GROUPECOURS FOR EACH ROW
 BEGIN
 DELETE FROM INSCRIPTION
 WHERE SIGLE =:OLD.SIGLE AND NOGROUPE =:OLD.NOGROUPE AND CODESESSION =:OLD.CODESESSION;
 dbms_output.put_line ('Les etudiants inscrits a ce cours sont automatiquement supprimes.');
 END;
/
/* C5CoursSuprimé TEST1 «DELETE-TRUE» */

/* Les étudiants inscrits à ce cours sont automatiquement SUPPRIMÉS */

DELETE FROM GROUPECOURS WHERE SIGLE = 'INF3180' and NOGROUPE = 30 AND CODESESSION = 32003;

/* Contrainte C6: C6UnCoursParSession */

 CREATE OR REPLACE TRIGGER C6UnCoursParSession
 BEFORE INSERT OR UPDATE ON GROUPECOURS FOR EACH ROW
 DECLARE
 nbFois INTEGER;
 BEGIN
 SELECT COUNT(SIGLE) INTO nbFois FROM GROUPECOURS
 WHERE SIGLE =:NEW.SIGLE AND CODESESSION=:NEW.CODESESSION;
 IF nbFois >= 1 THEN
 raise_application_error (-20507,'Un cours ne peut etre donne plus d’une fois pendant la meme session.');
 END IF;
 END;
/
/* C6UnCoursParSession TEST1 «INSERT-TRUE» */

/* Insertion pour la PREMIÈRE FOIS du cours INF2160 pendant la session 32003 */

INSERT INTO GroupeCours VALUES('INF2160',10,32003,100,'TREJ4');

/* C6UnCoursParSession TEST2 «INSERT-FALSE» */

/* Insertion PLUS D'UNE FOIS du cours INF2160 pendant la session 32003 */

INSERT INTO GroupeCours VALUES('INF2160',60,32003,100,'TREJ4');

/* Contrainte C7: C7NoteCinqPourcent */

 CREATE OR REPLACE TRIGGER C7NoteCinqPourcent
 BEFORE UPDATE OF NOTE ON INSCRIPTION FOR EACH ROW
 WHEN (NEW.NOTE - OLD.NOTE > OLD.NOTE * 5 / 100 )
 BEGIN raise_application_error(-20500, 'La note ne peut pas etre augmenter plus de 5%');
 END;
/

/* C7NoteCinqPourcent TEST1 «UPDATE-FALSE» */

/* Augmentation de la valeur de la note de PLUS DE 5% lors d’une mise à jour */

UPDATE INSCRIPTION SET NOTE = 90 WHERE
CODEPERMANENT = 'TREJ18088001' AND CODESESSION = 32003 AND SIGLE = 'INF1110';

/* C7NoteCinqPourcent TEST2 «UPDATE-TRUE» */

/* Augmentation de la valeur de la note de MOINS DE 5% lors d’une mise à jour */

UPDATE INSCRIPTION SET NOTE = 71 WHERE
CODEPERMANENT = 'TREJ18088001' AND CODESESSION = 32003 AND SIGLE = 'INF1130';

/* Contrainte C8: C8NbAbandons */

/* Ajoute de la colonne nbAbandons de type INTEGER qui permet les NULL */

ALTER TABLE GROUPECOURS ADD nbAbandons INTEGER NULL;

/* Contrainte C8, PAS de valeur négative */

 CREATE OR REPLACE TRIGGER C8NbAbandon
 BEFORE INSERT OR UPDATE ON GROUPECOURS FOR EACH ROW
 WHEN (NEW.nbAbandons < 0 )
 BEGIN raise_application_error(-20510, 'PAS de valeur negative pour le nbAbandons');
 END;
/

/* C8NbAbandons TEST1 «UPDATE-FALSE» */

/* La valeur de la colonne nbAbandons EST NÉGATIVE */

UPDATE GROUPECOURS SET nbAbandons = -10
WHERE SIGLE LIKE 'INF3123' AND CODEPROFESSEUR LIKE 'GOLA1';


/* C8NbAbandons TEST2 «UPDATE-FALSE» */

/* La valeur de la colonne nbAbandons EST NÉGATIVE */

UPDATE GROUPECOURS SET nbAbandons = -9
WHERE SIGLE LIKE 'INF1110' AND CODEPROFESSEUR LIKE 'TREJ4';
