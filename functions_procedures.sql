ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY'
/
SET SERVEROUTPUT ON
/
SET ECHO ON
/

/* C2 Triggers sur Vue */

 DROP VIEW MoyenneParGroupeParSession; 
 CREATE OR REPLACE VIEW MoyenneParGroupeParSession (SIGLE, NOGROUPE, CODESESSION, MOYENNE_NOTES) AS 
 SELECT      SIGLE, NOGROUPE, CODESESSION, AVG (NOTE) AS MOYENNE_NOTES
 FROM        INSCRIPTION
 GROUP BY    SIGLE, NOGROUPE, CODESESSION 
/

/* 2.2) Affichage de la vue */
 
 SELECT * FROM MoyenneParGroupeParSession
/

/* 2.3) Trigger de la vue: modifierVue */

 CREATE OR REPLACE TRIGGER modifierVue
 INSTEAD OF UPDATE ON MoyenneParGroupeParSession FOR EACH ROW
 BEGIN
 IF (:NEW.MOYENNE_NOTES <> :OLD.MOYENNE_NOTES ) THEN
 UPDATE INSCRIPTION SET NOTE = NOTE + (:NEW.MOYENNE_NOTES - :OLD.MOYENNE_NOTES) 
 WHERE SIGLE = :NEW.SIGLE AND NOGROUPE = :NEW.NOGROUPE AND CODESESSION = :NEW.CODESESSION;
 dbms_output.put_line ('Les notes des etudiants du groupe ont ete mis a jour equitablement'); 
 END IF;
 END;
/

   /* 2.4.1) augmentation de 10 de la moyenne du cours INF1110, groupe 20 de la session 32003 */

   UPDATE MoyenneParGroupeParSession SET MOYENNE_NOTES = MOYENNE_NOTES + 10
   WHERE SIGLE LIKE 'INF1110' AND  NOGROUPE = 20 AND CODESESSION = 32003;
   

   /* 2.4.2) l’affichage de la vue MoyenneParGroupeParSession pour le groupe modifie */

   SELECT * FROM MoyenneParGroupeParSession 
   WHERE SIGLE LIKE 'INF1110' AND  NOGROUPE = 20 AND CODESESSION = 32003;
   

   /* 2.4.3) l’affichage des lignes affectees par la mise à jour de la vue */
   
   SELECT CODEPERMANENT, NOTE FROM INSCRIPTION 
   WHERE SIGLE LIKE 'INF1110' AND  NOGROUPE = 20 AND CODESESSION = 32003;
   

/* C3-PACKAGE - Procédure et Fonction PL/SQL */

/* Déclaration des sous-programmes appelables de l'extérieur - PACKAGE */

CREATE OR REPLACE PACKAGE TP2_C3 AS
FUNCTION LibreEnseignement (unCodeProfesseur GROUPECOURS.CODEPROFESSEUR%TYPE, unCodeSession GROUPECOURS.CODESESSION%TYPE)
RETURN BOOLEAN; 
PROCEDURE TacheProfesseur (unCodeProfesseur GROUPECOURS.CODEPROFESSEUR %TYPE, unCodeSession 
GROUPECOURS.CODESESSION %TYPE);
END TP2_C3;
/
CREATE OR REPLACE PACKAGE BODY TP2_C3 AS

/* 3.1) Définition de la fonction «LibreEnseignement» spécifiés dans le package  */
 
FUNCTION LibreEnseignement (unCodeProfesseur GROUPECOURS.CODEPROFESSEUR%TYPE, unCodeSession 
GROUPECOURS.CODESESSION%TYPE)
RETURN BOOLEAN
IS
leCodeProfesseur GROUPECOURS.CODEPROFESSEUR%TYPE;
BEGIN
   SELECT DISTINCT
      CODEPROFESSEUR INTO leCodeProfesseur
   FROM
      GROUPECOURS
   WHERE
      CODEPROFESSEUR LIKE unCodeProfesseur AND CODESESSION = unCodeSession;
IF leCodeProfesseur = unCodeProfesseur THEN
   RETURN FALSE;
END IF;
EXCEPTION WHEN NO_DATA_FOUND THEN
 dbms_output.put_line ('le professeur ' || unCodeProfesseur || ' n’a aucune tache d’enseignement pendant la session donnee ');
RETURN TRUE;
END;

/* 3.2) Définition de la procédure «TacheProfesseur» spécifiés dans le package */

PROCEDURE TacheProfesseur (unCodeProfesseur GROUPECOURS.CODEPROFESSEUR%TYPE, unCodeSession GROUPECOURS.CODESESSION%TYPE)
IS
BEGIN
DECLARE
reponse BOOLEAN;
BEGIN reponse := LibreEnseignement(unCodeProfesseur, unCodeSession);
IF (reponse = TRUE) THEN
BEGIN
DECLARE
 leCodeProfesseur GROUPECOURS.CODEPROFESSEUR%TYPE;
BEGIN
   SELECT DISTINCT
      CODEPROFESSEUR INTO leCodeProfesseur
   FROM
      PROFESSEUR
   WHERE
      CODEPROFESSEUR LIKE unCodeProfesseur;
IF leCodeProfesseur = unCodeProfesseur THEN
   dbms_output.put_line('le professeur ' || unCodeProfesseur || ' est disponible pendant la session ' || unCodeSession);
END IF;
EXCEPTION WHEN NO_DATA_FOUND THEN
DBMS_OUTPUT.PUT_LINE ('le professeur ' || unCodeProfesseur || ' n’a pas ete trouve dans la base de donnees');
END;
END;
ELSE
   dbms_output.put_line('le professeur ' || unCodeProfesseur || ' enseigne les cours: ' ); 
FOR x IN (SELECT
      SIGLE
   FROM
      GROUPECOURS
   WHERE
      CODEPROFESSEUR LIKE unCodeProfesseur AND CODESESSION = unCodeSession)
LOOP
dbms_output.put_line(x.SIGLE);
END LOOP;
dbms_output.put_line('pendant la session ' || unCodeSession || ' pour les groupes: ' );
FOR x IN (SELECT
      NOGROUPE
   FROM
      GROUPECOURS
   WHERE
      CODEPROFESSEUR LIKE unCodeProfesseur AND CODESESSION = unCodeSession)
LOOP 
dbms_output.put_line(x.NOGROUPE);
END LOOP;
END IF;
END;
END;
END TP2_C3;
/

    /* 3.3) Tests: accéder aux objets du package */
BEGIN
  TP2_C3.TACHEPROFESSEUR ('TREJ4', 32003);
  TP2_C3.TACHEPROFESSEUR ('SAUV5', 32003);
  TP2_C3.TACHEPROFESSEUR ('ABCD7', 32003);
END;
/

