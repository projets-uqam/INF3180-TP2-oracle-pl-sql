INF3180-TP2-oracle-pl/sql
=========================

Summary
---------

* Creating Triggers, some examples
* Using the Package Elements (variables, procedures or functions)


Technologies used:
------------------

* oracle-ex-11.2.0
* pl/sql


Database
---------

Script to create the database [script_bd.sql](script_bd.sql)


Reference
----------

INF3180 | 30 | Automme 2016 | Professeur: Fatiha Sadat


License
--------

This project is licensed under the Apache License - see [here](https://www.gnu.org/licenses/gpl-3.0.en.html) for details
